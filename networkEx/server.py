#!/usr/bin/env python3

import socket

def serverRun(s):
    
    host = socket.getfqdn()
    port = 5677

    server_ip = socket.gethostbyname(host)

    s.bind((server_ip, port))
    

def GameRules(a1, a2):
    if a1 == 'p' and a2 == 'r':
        return '1'
    elif a1 == 'p' and a2 == 's':
        return '2'
    elif a1 == 's' and a2 == 'p':
        return '1'
    elif a1 == 's' and a2 == 'r':
        return '2'
    elif a1 == 'r' and a2 == 's':
        return '1'
    elif a1 == 'r' and a2 == 'p':
        return '2'
    else:
        return '3'

if __name__ == '__main__':        
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serverRun(s)

    print('Waiting for 2 players...')
    s.listen()
    conn1, addr1 = s.accept()
    print('Player1 Connected: ', addr1)
   #conn1.send(str.encode('NotReady'))

    s.listen()
    conn2, addr2 = s.accept()
    print('Player2 Connected: ', addr2)
   #conn1.send(str.encode('Ready'))
   #conn2.send(str.encode('Ready'))

    answer1 = conn1.recv(1024)
    answer1 = answer1.decode('utf-8')
    answer2 = conn2.recv(1024)
    answer2 = answer2.decode('utf-8')

    reply = GameRules(answer1, answer2)
    if reply == '1':
        conn1.send(str.encode('You win!'))
        conn2.send(str.encode('You lose'))
    elif reply == '2':
        conn2.send(str.encode('You win!'))
        conn1.send(str.encode('You lose'))
    elif reply == '3':
        conn1.send(str.encode('Draw'))
        conn2.send(str.encode('Draw'))
    else:
        print('Error')
