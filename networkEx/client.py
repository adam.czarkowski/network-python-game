#!/usr/bin/env python3

import socket 
import tkinter as tk
from PIL import ImageTk, Image

def Connect():
    global ip, iphost, s
    iphost = ip
    iphost = socket.gethostbyname(iphost)
    port = 5677
    #try:
    s.connect((iphost, port))
    GameRun()
   #except:
   #     Error()

def Wait():
    waittext = tk.Label(canvas, text="Waiting for opponent...", font=('Helvetica', '20'))
    waittext.place(relx=0.00, rely=0.40, relwidth=1.00, relheight=0.20)
    data = s.recv(5)
    data = data.decode('utf-8')
    if data == 'Ready':
        waittext.destroy()
        GameRun()
    
def ConnFun():
    global ip, entrytext, entrybk, entry, ok
    ip = ''
    ButtonHost.destroy()
    ButtonConn.destroy()
    ButtonExit.destroy() 
    
    entrytext = tk.Label(canvas, text="Connect via ipv4:", font=('Helvetica', '20'))
    entrytext.place(relx=0.00, rely=0.20, relwidth=1.00, relheight=0.20)
    entrybk = tk.Label(canvas)
    entrybk.place(relx=0.00, rely=0.40, relwidth=1.00, relheight=0.40)
    entry = tk.Entry(canvas, font=('Helvetica', '20'), background='lightgray') 
    entry.place(relx=0.15, rely=0.40, relwidth=0.70, relheight=0.15)
    ok = tk.Button(canvas, text='OK', font=('Helvetica', '25'), command=lambda: ConnDestroy())
    ok.place(relx=0.20, rely=0.60, relwidth=0.60, relheight=0.15)
    
def ConnDestroy():
    global entrytext, entrybk, entry, ok, ip
    ip = entry.get()
    entrytext.destroy()
    entrybk.destroy()
    entry.destroy()
    ok.destroy()
    Connect()
    
def HostFun():
    global ip
    ButtonHost.destroy()
    ButtonConn.destroy()
    ButtonExit.destroy() 
    ip = socket.getfqdn()
    Connect()

def Exit():
    root.destroy()

def GameRun():
    global RockButton, PaperButton, ScisButton, PickText
    
    #rockIMG = tk.PhotoImage(file = r'kamien.png')
    #paperIMG = tk.PhotoImage(file = r'papier.png')
    #scissIMG = tk.PhotoImage(file = r'nozyczki.png')
            
    PickText = tk.Label(canvas, text="Pick something!", font=('Helvetica', '20'), background='grey')
    PickText.place(relx=0.00, rely=0.05, relwidth=1.00, relheight=0.15)
    RockButton = tk.Button(canvas, text="Rock", font=('Helvetica', '25'), command=lambda: Answering.SendRock())
    RockButton.place(relx=0.10, rely=0.25, relwidth=0.80, relheight=0.20)
    PaperButton = tk.Button(canvas, text="Paper", font=('Helvetica', '25'), command=lambda: Answering.SendPaper())
    PaperButton.place(relx=0.10, rely=0.50, relwidth=0.80, relheight=0.20)
    ScisButton = tk.Button(canvas, text="Scissors", font=('Helvetica', '25'), command=lambda: Answering.SendScis())
    ScisButton.place(relx=0.10, rely=0.75, relwidth=0.80, relheight=0.20)

class Answering:
    def SendRock():
        s.sendall(str.encode('r'))
        Listen()
    def SendPaper():
        s.sendall(str.encode('p'))
        Listen()
    def SendScis():
        s.sendall(str.encode('s'))
        Listen()
        
def Listen():
    global RockButton, PaperButton, ScisButton, PickText
    RockButton.destroy()
    PaperButton.destroy()
    ScisButton.destroy()
    PickText.destroy()
    answer = s.recv(100)
    answer = answer.decode('utf-8')
    score = tk.Label(canvas, text=answer, font=('Helvetica', '25'))
    score.place(relx=0.00, rely=0.40, relwidth=1.00, relheight=0.20)
        
def Error():
    global iphost
    error = tk.Label(canvas, text=iphost)
    error.place(relx=0.00, rely=0.40, relwidth=1.00, relheight=0.40)
    
if __name__ == "__main__":

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    root = tk.Tk()
    root.title('RPS')

    canvas = tk.Canvas(root, width=350, height=500, background='lightgreen')
    canvas.pack()
    
    ButtonConn = tk.Button(canvas, text='CONNECT', font=('Helvetica', '25'), command=lambda: ConnFun())
    ButtonConn.place(relx=0.20, rely=0.40, relwidth=0.60, relheight=0.15)

    ButtonHost = tk.Button(canvas, text='HOST', font=('Helvetica', '25'), command=lambda: HostFun())
    ButtonHost.place(relx=0.20, rely=0.60, relwidth=0.60, relheight=0.15)

    ButtonExit = tk.Button(canvas, text='EXIT', font=('Helvetica', '25'), command=lambda: Exit())
    ButtonExit.place(relx=0.20, rely=0.80, relwidth=0.60, relheight=0.15)
    
    root.mainloop()